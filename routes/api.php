<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\AuthController@login')->name('login');
Route::post('register', 'Auth\AuthController@register');
Route::get('register', 'Auth\AuthController@register');
Route::get('login', 'Auth\AuthController@login')->name('login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', 'Auth\AuthController@logout');
//          Route::get('user', 'Auth\AuthController@user');
});
//Route::resource('products',ProductController::class);

Route::get('/products', 'ProductController@index');
Route::get('/loyalty','LoyaltyCustomerController@index');
Route::get('/getPoint','LoyaltyCustomerController@index');
Route::get('/getTransaction','LoyaltyCustomerController@index');
Route::get('/partners','PartnersController@index');
Route::get('/promos','PromosController@index');
Route::post('promoMsg','PromoMsgController@index');

Route::post('getUnread','PromoMsgController@getUnread');
Route::post('linkCard','LoyaltyCustomerController@linkCard');
Route::post('updateStatusPromo','PromoMsgController@updateStatus');
Route::get('updateProfile','Auth\AuthController@updateProfile');
Route::get('authenticate','Auth\AuthController@authenticate');
Route::get('updatePassword','Auth\AuthController@updatePassword');
Route::post('verifyLinkCode','LinkCodeVerificationController@verifyLinkCode');
Route::get('user','LinkCodeVerificationController@index');
Route::get('services','ServicesController@index');
Route::get('applyProgram','ApplicationController@applyProgram');
Route::get('getApplication','ApplicationController@getApplication');
Route::get('getEvent','EventController@index');





//Route::apiResource('/products', 'Api\ProductController');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoyaltyCustomer extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='loyalty_customers';
    protected $protected = ['id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointsHistory extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='points';
    protected $protected = ['id'];

    protected $casts = [
      'created_at' => 'datetime:Y-m-d H:i:s',
        'points' => 'integer',
    ];
}

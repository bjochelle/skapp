<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promos extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='promos';
    protected $protected = ['id'];

}

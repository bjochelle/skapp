<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivedPromo extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='archive_promo';
    protected $fillable  = ['promo_id','user_id','isRead','isPromoArchive'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkCodeVerification extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='link_code_verifications';
    protected $protected = ['id'];
}

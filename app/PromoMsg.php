<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoMsg extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='promotions';
    protected $protected = ['id'];
}

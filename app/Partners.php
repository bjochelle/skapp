<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    protected $connection ='sqlsrv';
    protected $table  ='partners';
    protected $protected = ['id'];
}

<?php
namespace App\Http\Controllers\Auth;
use App\Event;
use App\LoyaltyCustomer;
use App\Programs;
use App\Services;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AuthController extends Controller
{
    //
    public function login(Request $request) {


        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ],401);
        $user = $request->user();

        $services = Services::get();
        $programs = Programs::get();

        $event = Event::join('programs as p','p.id','events.program_id')
            ->orderby('event_date_from','desc')->get();

     return response()->json([
         'user_data' => $user,
         'services' => $services,
         'programs' => $programs,
         'event' => $event,
         'token_type' => 'Bearer',
     ]);
   }
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
                 'password' => 'required|string'
          ]);
          $user = new User;
          $user->fullname = $request->name;
          $user->contactNo = $request->mobileNumber;
          $user->username = $request->email;
          $user->email = $request->email;
          $user->dob = date(('Y-m-d'),strtotime($request->dob));
          $user->password = bcrypt($request->password);
          $user->role_id = 'U';
          $user->save();



        return response()->json([
            'status' => 'Success',
              'message' => 'Successfully created user!'
          ], 201);
   }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email'=>'required|unique:users,email,'.$request->id,
            'contactNo' => 'required|unique:users,contactNo,'.$request->id
        ]);
        $user = User::where('id',$request->id)->first();
        $user->fullname = $request->name;
        $user->contactNo = $request->contactNo;
        $user->dob = date(('Y-m-d'),strtotime($request->dob));
        $user->email = $request->email;
        $user->save();

        return response()->json([
            'status' => 'Success',
            'user_data' => $user
        ]);
    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
            ]);
    }
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function updatePassword(Request $request)
    {


        $request->validate([
            'id' => ['required'],
            'password' => ['required'],
        ]);


        $user = User::where('id', $request->id)->first();
        $user->password = bcrypt($request->password);
        $user->save();

        if($user){
            return response()->json([
                'user_data' => $user,
                'status' => 'Success',
                'subHeader' => 'Update Success',
                'message' => 'Your password successfully updated.',
            ]);
        }else{
            return response()->json([
                'user_data' => $user,
                'status' => 'Error',
                'subHeader' => 'Update Failed',
                'message' => 'Something went wrong',
            ]);
        }



    }

    public function authenticate(Request $request)
    {

        $request->validate([
            'id' => ['required'],
            'password' => ['required'],
        ]);


        $user = User::where('id', $request->id)->first();

        if(!$user){
            return response()->json([
                'status' => 'error',
                'message' => "User Not found!. Please re-login.",
            ]);
        }else{
            if(Hash::check($request->password, $user->password)){
                return response()->json([
                    'status' => 'Success',
                    'message' => "Password Match",
                ]);
            }else{
                return response()->json([
                    'status' => 'Error',
                    'message' => "Unmatched Password",
                ]);
            }

        }
    }
}

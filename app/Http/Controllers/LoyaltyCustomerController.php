<?php

namespace App\Http\Controllers;

use App\LinkCodeVerification;
use App\LoyaltyCustomer;
use App\PointsHistory;
use App\Redeem;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class LoyaltyCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if($request->getPoints){
            $query = LoyaltyCustomer::where('card_number',$request->cardNumber)->first();
        }else if($request->getTransaction){
            $findCard = LoyaltyCustomer::where('card_number',$request->cardNumber)->first();
            $getPoints = PointsHistory::where('card_number',$request->cardNumber)
                ->orderBy('created_at','DESC')
                ->select('points.*',DB::raw("'S' as status"))->get();

            $myArrayPoints = json_decode(json_encode($getPoints), true);
            $getRedeem = Redeem::where('card_number',$request->cardNumber)
                ->orderBy('created_at','DESC')
                ->select('redeemed.*',DB::raw("'R' as status"))->get();

            $myArrayRedeen = json_decode(json_encode($getRedeem), true);
            $query = array_merge($myArrayRedeen, $myArrayPoints);
            usort($query, function ($a, $b) {
                if($a['created_at']==$b['created_at']) return 0;
                return $a['created_at'] < $b['created_at']?1:-1;
            });
        }

//        return response()->json([
//            'transactions' => $query,
//            'cardInfo' => $findCard,
//        ], 201);

        return response()->json($query);
//        $query = LoyaltyCustomer::orderBy('full_name','asc')->get();
//        return response()->json($query);
    }

    public function linkCard(Request  $request)
    {
        if($request->status){
            $user = User::where('cardNumber',$request->cardNumber)->first();
            $user->cardNumber = null;
            $user->save();

            if($user){
                return response()->json([
                    'status' => 'Success',
                    'subHeader' => 'Unlinked Success',
                    'message' => 'Your card number successfully unlinked.',
                    'user_data' => $user,
                ], 201);
            }else{
                return response()->json([
                    'status' => 'Error',
                    'subHeader' => 'Unlinked Error',
                    'message' => 'Something went wrong'
                ], 201);
            }

        }else{
            $cn = $request->cardNumber;
            $findCard = LoyaltyCustomer::where('card_number',$cn)->first();
            if($findCard) {
                $checkLink = User::where('cardNumber', $cn)->first();
                if ($checkLink) {
                    return response()->json([
                        'status' => 'Error',
                        'subHeader' => 'Linked Error',
                        'message' => 'Card Number is already linked to other user'
                    ], 201);
                } else {
                    $user = User::where('id', $request->userId)->first();

//                    $user->cardNumber = $cn;
//                    $user->save();

//                    $getPoints = PointsHistory::where('card_number', $cn)
//                        ->orderBy('created_at', 'DESC')
//                        ->select('points.*', DB::raw("'S' as status"))->get();
//
//                    $myArrayPoints = json_decode(json_encode($getPoints), true);
//                    $getRedeem = Redeem::where('card_number', $cn)
//                        ->orderBy('created_at', 'DESC')
//                        ->select('redeemed.*', DB::raw("'R' as status"))->get();
//
//                    $myArrayRedeen = json_decode(json_encode($getRedeem), true);
//                    $query = array_merge($myArrayRedeen, $myArrayPoints);
//                    usort($query, function ($a, $b) {
//                        if ($a['created_at'] == $b['created_at']) return 0;
//                        return $a['created_at'] < $b['created_at'] ? 1 : -1;
//                    });

                    do{
                        (string)$verCode = rand(100000,999999);
                        (string)$other_redeem_code = '';
                        $other_code = LinkCodeVerification::where('code', $verCode)
                            ->whereNull('isExpired')->first();
                        if( $other_code != null ) {
                            $verCode= $other_code ->verCode;
                        }
                    }while($verCode == $other_redeem_code);

                    $code = new LinkCodeVerification();
                    $code->user_id =  $request->userId;
                    $code->cardNumber = $cn;
                    $code->code= $verCode;
                    $code->save();


                    $html = '<b>Hi <span style="text-transform: uppercase">' . $user->name . '</span>,</b>' .
                        '<br><br>Alert: You\'re linking your card into your Builder\'s Reward account.' .
                        '<br><br>Use this verification code to perform this action:<br><br><h1>'.$verCode.'</h1>' .
                        '<br>DO NOT SHARE THE CODE WITH ANYONE. If you did not make this request,
                            change your password immediately and contact Builders Reward support.' .
                        '<br><br><b>Note on account security:</b> Builders Reward will <b>NEVER</b> ask for your password and verification code via email, text or call.' .
                        '<br><br><br>Regards,<br><br>The Builder\'s Reward Team';


                    Mail::send([], [], function ($message) use ($html, $user) {
                        $message->to($user->email, $user->name)->subject
                        ('Link Card Verification Code')
                            ->setBody($html, 'text/html');
                        $message->from('xyz@gmail.com', "Builder's Reward");
                    });

                    if ($user) {
                        return response()->json([
                            'status' => 'Success',
                            'subHeader' => 'Linked Success',
                            'message' => 'Your card number successfully linked.',
//                            'transactions' => $query,
//                            'cardInfo' => $findCard,
                        ], 201);
                    } else {
                        return response()->json([
                            'status' => 'Error',
                            'subHeader' => 'Linked Error',
                            'message' => 'Something went wrong'
                        ], 201);
                    }
                }
            }else{
                return response()->json([
                    'status' => 'Error',
                    'subHeader' => 'Linked Error',
                    'message' => 'Card Number not found. Make sure it is Builders\'s Reward Card.',
                ], 201);
            }
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoyaltyCustomer  $loyaltyCustomer
     * @return \Illuminate\Http\Response
     */
    public function show(LoyaltyCustomer $loyaltyCustomer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoyaltyCustomer  $loyaltyCustomer
     * @return \Illuminate\Http\Response
     */
    public function edit(LoyaltyCustomer $loyaltyCustomer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoyaltyCustomer  $loyaltyCustomer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoyaltyCustomer $loyaltyCustomer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoyaltyCustomer  $loyaltyCustomer
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoyaltyCustomer $loyaltyCustomer)
    {
        //
    }
}

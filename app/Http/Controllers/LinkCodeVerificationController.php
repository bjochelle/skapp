<?php

namespace App\Http\Controllers;

use App\LinkCodeVerification;
use App\LoyaltyCustomer;
use App\PointsHistory;
use App\Redeem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LinkCodeVerificationController extends Controller
{

    public function index(){
        $query = User::get();
        return $query;
    }

    public function verifyLinkCode(Request $request)
    {

        $code = LinkCodeVerification::where('user_id',$request->userId)
            ->where('cardNumber',$request->cardNumber)
            ->where('code',$request->code)
            ->whereNull('isExpired')->first();
        if($code){
            $user = User::where('id', $request->userId)->first();
            $user->cardNumber = $request->cardNumber;
            $user->save();

            $findCard = LoyaltyCustomer::where('card_number',$request->cardNumber)->first();

            $getPoints = PointsHistory::where('card_number', $request->cardNumber)
                ->orderBy('created_at', 'DESC')
                ->select('points.*', DB::raw("'S' as status"))->get();

            $myArrayPoints = json_decode(json_encode($getPoints), true);
            $getRedeem = Redeem::where('card_number', $request->cardNumber)
                ->orderBy('created_at', 'DESC')
                ->select('redeemed.*', DB::raw("'R' as status"))->get();

            $myArrayRedeen = json_decode(json_encode($getRedeem), true);
            $query = array_merge($myArrayRedeen, $myArrayPoints);
            usort($query, function ($a, $b) {
                if ($a['created_at'] == $b['created_at']) return 0;
                return $a['created_at'] < $b['created_at'] ? 1 : -1;
            });

            $updateCode = LinkCodeVerification::where('user_id',$request->userId)
                ->where('cardNumber',$request->cardNumber)
                ->where('code',$request->code)
                ->whereNull('isExpired')->first();
            $updateCode->isExpired = 1;
            $updateCode->save();

            return response()->json([
                'status' => 'Success',
                'subHeader' => 'Linked Success',
                'message' => 'Your card number successfully linked.',
                'transactions' => $query,
                'cardInfo' => $findCard,
            ], 201);
        }else{

            $uc = LinkCodeVerification::where('user_id',$request->userId)
                ->where('cardNumber',$request->cardNumber)
                ->whereNull('isExpired')->first();
            $uc->isExpired = 1;
            $uc->save();

            return response()->json([
                'status' => 'Error',
                'subHeader' => 'Linked Error',
                'message' => "Verification Code is error.",
            ], 201);
        }

    }
}

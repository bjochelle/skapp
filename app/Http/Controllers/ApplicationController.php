<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function  applyProgram(Request $request){

        $checkApplication = Application::where('user_id',$request->userId)
            ->where('service_id',$request->serviceId)
            ->where('program_id',$request->id)->first();
        if($checkApplication){
            return response()->json([
                'status' => 'Error',
                'message' => 'You have already applied in this program!'
            ], 201);
        }else{
            $prog = new Application();
            $prog->user_id = $request->userId;
            $prog->service_id = $request->serviceId;
            $prog->program_id = $request->id;
            $prog->note = '';
            $prog->status = 0;
            $prog->save();

            $list = Application::where('user_id',$request->userId)
            ->join('programs as p','p.id','applications.program_id')->get();
            return response()->json([
                'status' => 'Success',
                'app'=>$list,
                'message' => 'Successfully applied!'
            ], 201);
        }

    }

    public function  getApplication(Request $request)
    {

        $list = Application::where('user_id', $request->id)
            ->join('programs as p', 'p.id', 'applications.program_id')
            ->select('applications.status as status', 'program_name', 'program_desc', 'applications.id as id')->get();
        return response()->json([
            'status' => 'Success',
            'app' => $list,
            'message' => 'Successfully!'
        ], 201);
    }
}

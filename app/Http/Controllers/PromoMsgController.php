<?php

namespace App\Http\Controllers;

use App\ArchivedPromo;
use App\PromoMsg;
use Illuminate\Http\Request;

class PromoMsgController extends Controller
{
    public function index(Request $request)
    {
        if ($request->detailed === 'true') {
            $url ='http://176.16.16.121/vcy-pos1/public';
            $query = PromoMsg::where('isArchived', '0')
                ->where('id', $request->userId)->first();
            $query['source'] = $url . $query->filename;
        } else {
            $fetchPromo = PromoMsg::where('isArchived', '0')
                ->get();
            $query = $fetchPromo;
            foreach ($fetchPromo as $key => $value){
                $promo = ArchivedPromo::where('user_id',$request->userId)
                    ->where('promo_id',$value->id)->first();
                $query[$key]['data']=$promo;
                if($promo){
                    if($promo->isPromoArchive && $promo->isRead) {
                        $query[$key]['isPromoArchive'] = '1';
                        $query[$key]['isRead'] = '1';
                    }else if($promo->isRead){
                        $query[$key]['isPromoArchive'] = '0';
                        $query[$key]['isRead'] = '1';
                    }else{
                        $query[$key]['isPromoArchive'] = '1';
                        $query[$key]['isRead'] = '0';
                    }
                }else{
//                    $query[$key]['isPromoArchive'] = '0';
//                    $query[$key]['isRead'] = '0';
                }
            }
//            $query = PromoMsg::where('isArchived', '0')
//                ->whereNull('user_id')
//                ->leftJoin('archive_promo as ap', 'promotions.id', 'ap.promo_id')
//                ->select('promotions.id as id', 'ap.*', 'promotions.*')
//                ->get();
        }

        return $query;
    }

        public function getUnread(Request $request)
    {
//        $countPromo = PromoMsg::where('isArchived', '0')->count();
//
//        $readPromo = ArchivedPromo::where('user_id',$request->userId)->count();
//
//       $unread = intval($countPromo) - intval($readPromo);

        $unread = PromoMsg::where('isArchived', '0')
            ->whereNull('user_id')
            ->leftJoin('archive_promo as ap', 'promotions.id', 'ap.promo_id')->count();
        return $unread;

    }

    public function updateStatus(Request $request)
    {
        $status = $request->status;
        if($status === 'archived') {
            $update = ArchivedPromo::updateOrCreate(
                ['promo_id' => $request->id,
                    'user_id' => $request->userId],
                ['promo_id' => $request->id,
                    'user_id' => $request->userId,
                    'isPromoArchive' => 1,
                ]);
        }else {
            $update = ArchivedPromo::updateOrCreate(
                ['promo_id' => $request->id,
                    'user_id' => $request->userId],
                ['promo_id' => $request->id,
                    'user_id' => $request->userId,
                    'isRead' => 1,
                ]);
        }

            if($update){
                return response()->json([
                    'status' => 'Success',
                    'subHeader' => 'Update Success',
                    'message' => 'Promo is successfully updated.',
                ], 201);
            }else{
                return response()->json([
                    'status' => 'Error',
                    'subHeader' => 'Archived Error',
                    'message' => 'Something went wrong'
                ], 201);
            }
    }
}

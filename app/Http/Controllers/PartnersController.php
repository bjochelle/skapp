<?php

namespace App\Http\Controllers;

use App\Partners;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index(Request $request)
    {
        $query = Partners::where('isArchived', '0')->get();
        foreach ($query as $key => $value){
//            $url = env('APP_URL').'vcy-pos1/public';
            $url ='http://176.16.16.121/vcy-pos1/public';

            $value['source'] = $url.$value->filename;
        }
        return $query;
    }
}

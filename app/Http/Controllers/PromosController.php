<?php

namespace App\Http\Controllers;

use App\PromoMsg;
use App\Promos;
use Illuminate\Http\Request;

class PromosController extends Controller
{

    public function index(Request $request)
    {
        $query = PromoMsg::where('isArchived', '0')->get();
        $count = count($query);
        foreach ($query as $key => $value){
            $value['source'] = 'http://176.16.16.121/vcy-pos1/public'.$value->filename;
        }
        return $query;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redeem extends Model
{

    protected $table  ='redeemed';
    protected $protected = ['id'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'points' => 'integer',
    ];
}

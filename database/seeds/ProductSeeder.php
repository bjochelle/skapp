<?php

namespace Database\Seeders;

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Test Product',
            'price' => 200.50,
            'category' => 'mobile',
            'description' => 'Samsung Galaxy a21S 64GB dUAL sIM',
            'image' => 'https://www.pinoytechnoguide.com/wp-content/uploads/2021/04/Xiaomi-Redmi-Note-10-Pro.jpg',
        ]);

//        Product::create([
//            'name' => 'Test Product',
//            'price' => 200.50,
//            'category' => 'mobile',
//            'description' => 'Samsung Galaxy a21S 64GB dUAL sIM',
//            'image' => 'https://www.pinoytechnoguide.com/wp-content/uploads/2021/04/Xiaomi-Redmi-Note-10-Pro.jpg',
//        ]);

//        Product::create([
//            'name' => 'Test Product1',
//            'price' => 1200.50,
//            'category' => 'mobile',
//            'description' => 'Samsung Galaxy a21S 64GB dUAL sIM',
//            'image' => 'https://i.ebayimg.com/images/g/0tAAAOSwL1tfop1p/s-l1600.jpg',
//        ]);
    }
}
